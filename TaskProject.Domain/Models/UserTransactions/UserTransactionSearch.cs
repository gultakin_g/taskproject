﻿using TaskProject.Domain.Bases;

namespace TaskProject.Domain.Models.UserTransactions
{
    public class UserTransactionSearch : BaseSearch
    {
        public int? UserId { get; set; }
        public double? Amount { get; set; }
    }
}
