﻿using System;
using TaskProject.Domain.Bases;
using TaskProject.Domain.Models.Users;

namespace TaskProject.Domain.Models.UserTransactions
{
    public class UserTransaction : BaseEntity
    {
        public double Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public virtual User User { get; set; }
    }
}
