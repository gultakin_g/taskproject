﻿using System.Collections.Generic;
using TaskProject.Domain.Bases;
using TaskProject.Domain.Models.UserTransactions;

namespace TaskProject.Domain.Models.Users
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public virtual ICollection<UserTransaction> UserTransactions { get; set; }
    }
}
