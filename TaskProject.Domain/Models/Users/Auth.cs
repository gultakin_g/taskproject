﻿namespace TaskProject.Domain.Models.Users
{
    public class Auth
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
