﻿using TaskProject.Domain.Bases;

namespace TaskProject.Domain.Models.Users
{
    public class UserSearch : BaseSearch
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
