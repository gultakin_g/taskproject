﻿using System.ComponentModel.DataAnnotations;

namespace TaskProject.Domain.Bases
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
