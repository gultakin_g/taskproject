﻿namespace TaskProject.Domain.Bases
{
    public class BaseSearch
    {
        public int CurrentPage { get; set; }
        public int PerPage { get; set; }
    }
}
