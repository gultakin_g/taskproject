﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Application.Services.UserTransactions;
using TaskProject.Application.Services.UserTransactions.DTOs;

namespace TaskProject.Presentation.Controllers
{
    public class UserTransactionsController : BaseController
    {
        private readonly IUserTransactionService _userTransactionService;

        public UserTransactionsController(IUserTransactionService userTransactionService)
        {
            _userTransactionService = userTransactionService;
        }

        [HttpGet]
        public IActionResult FindList([FromQuery]UserTransactionSearchDTO userTransactionSearchDTO)
        {
            IList<UserTransactionDTO> userTransactionDTOs = _userTransactionService.FindList(userTransactionSearchDTO);
            return Ok(userTransactionDTOs);
        }

        [HttpGet]
        public IActionResult GetUserTransactionDetails(int id)
        {
            UserTransactionUpdateDTO userTransactionUpdateDTO = _userTransactionService.GetUserTransactionDetails(id);
            return Ok(userTransactionUpdateDTO);
        }

        [HttpPost]
        public IActionResult Insert([FromBody]UserTransactionInsertDTO userTransactionInsertDTO)
        {
            _userTransactionService.Insert(userTransactionInsertDTO);
            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody]UserTransactionUpdateDTO userTransactionUpdateDTO)
        {
            _userTransactionService.Update(userTransactionUpdateDTO);
            return Ok();
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            _userTransactionService.Delete(id);
            return Ok();
        }
    }
}
