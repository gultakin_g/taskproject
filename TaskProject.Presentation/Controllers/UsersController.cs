﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using TaskProject.Application.Services.Users;
using TaskProject.Application.Services.Users.DTOs;
using TaskProject.Application.Services.UserTransactions.DTOs;

namespace TaskProject.Presentation.Controllers
{
    public class UsersController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IConfiguration _configuration;

        public UsersController(IUserService userService,
            IConfiguration configuration)
        {
            _userService = userService;
            _configuration = configuration;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody] AuthDTO authDTO)
        {
            UserDTO userDTO = _userService.Authenticate(authDTO);
            if(userDTO == null)
            {
                return NotFound("Email and/or password is incorrect!");
            }
            string userToken = GenerateToken(userDTO);
            return Ok(new { User = userDTO, Token = userToken});
        }

        private string GenerateToken(UserDTO authenticatedUser)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] keyword = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("JwtSettings:Keyword"));
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Sid, authenticatedUser.Id.ToString()),
                    new Claim(ClaimTypes.NameIdentifier, authenticatedUser.Email.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(keyword), SecurityAlgorithms.HmacSha256Signature)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            string userToken = tokenHandler.WriteToken(token);
            return userToken;
        }

        [HttpGet]
        public IActionResult FindList([FromQuery] UserSearchDTO userSearchDTO)
        {
            IList<UserDTO> userDTOs = _userService.FindList(userSearchDTO);
            return Ok(userDTOs);
        }

        [HttpGet]
        public IActionResult GetUserDetails(int id)
        {
            UserUpdateDTO userUpdateDTO = _userService.GetUserDetails(id);
            return Ok(userUpdateDTO);
        }

        [HttpPost]
        public IActionResult Insert([FromBody]UserInsertDTO userInsertDTO)
        {
            _userService.Insert(userInsertDTO);
            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody]UserUpdateDTO userUpdateDTO)
        {
            _userService.Update(userUpdateDTO);
            return Ok();
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            _userService.Delete(id);
            return Ok();
        }

        [HttpGet]
        public IActionResult GetUserTransactions(int userId)
        {
            IList<UserTransactionDTO> userTransactionDTOs = _userService.GetUserTransactions(userId);
            return Ok(userTransactionDTOs.Select(e => new
            {
                Id = e.Id,
                Amount = e.Amount,
                Created = e.CreatedAt
            }));
        }
    }
}
