﻿using AutoMapper;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using TaskProject.Application;
using TaskProject.Application.Services.Users;
using TaskProject.Application.Services.Users.DTOs;
using TaskProject.Application.Services.UserTransactions;
using TaskProject.Application.Services.UserTransactions.DTOs;
using TaskProject.Application.Validators.Users;
using TaskProject.Application.Validators.UsersTransactions;
using TaskProject.Infrastructure.Repositories.Users;
using TaskProject.Infrastructure.Repositories.UserTransactions;

namespace TaskProject.Presentation.Extentions
{
    public static class ServiceExtention
    {
        public static void ResgisterCustomServices(this IServiceCollection services)
        {
            RegisterServices(services);
            RegisterRepository(services);
            RegisterValidator(services);
            RegisterMappers(services);
        }

        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserTransactionService, UserTransactionService>();
        }

        public static void RegisterRepository(IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserTransactionRepository, UserTransactionRepository>();
        }

        public static void RegisterValidator(IServiceCollection services)
        {
            services.AddTransient<IValidator<UserInsertDTO>, UserInsertValidator>();
            services.AddTransient<IValidator<UserUpdateDTO>, UserUpdateValidator>();

            services.AddTransient<IValidator<UserTransactionInsertDTO>, UserTransactionInsertValidator>();
            services.AddTransient<IValidator<UserTransactionUpdateDTO>, UserTransactionUpdateValidator>();
        }

        private static void RegisterMappers(IServiceCollection services)
        {
            MapperConfiguration mapperConfiguration = new MapperConfiguration(config =>
            {
                config.AddProfile(new MapperProfile());
            });
            IMapper mapper = mapperConfiguration.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
