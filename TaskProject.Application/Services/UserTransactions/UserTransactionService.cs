﻿using AutoMapper;
using System.Collections.Generic;
using TaskProject.Application.Services.UserTransactions.DTOs;
using TaskProject.Domain.Models.UserTransactions;
using TaskProject.Infrastructure.Repositories.UserTransactions;

namespace TaskProject.Application.Services.UserTransactions
{
    public class UserTransactionService : IUserTransactionService
    {
        private readonly IUserTransactionRepository _userTransactionRepository;
        private readonly IMapper _mapper;

        public UserTransactionService(
            IUserTransactionRepository userTransactionRepository,
            IMapper mapper)
        {
            _userTransactionRepository = userTransactionRepository;
            _mapper = mapper;
        }

        public IList<UserTransactionDTO> FindList(UserTransactionSearchDTO userTransactionSearchDTO)
        {
            UserTransactionSearch userTransactionSearch = _mapper.Map<UserTransactionSearch>(userTransactionSearchDTO);
            IList<UserTransaction> userTransactions = _userTransactionRepository.FindList(userTransactionSearch);
            IList<UserTransactionDTO> userTransactionDTO = _mapper.Map<IList<UserTransactionDTO>>(userTransactions);
            return userTransactionDTO;
        }

        public UserTransactionUpdateDTO GetUserTransactionDetails(int id)
        {
            UserTransaction userTransaction = _userTransactionRepository.GetUserTransactionDetails(id);
            UserTransactionUpdateDTO userTransactionUpdateDTO = _mapper.Map<UserTransactionUpdateDTO>(userTransaction);
            return userTransactionUpdateDTO;
        }

        public void Insert(UserTransactionInsertDTO userTransactionInsertDTO)
        {
            UserTransaction userTransaction = _mapper.Map<UserTransaction>(userTransactionInsertDTO);
            _userTransactionRepository.Insert(userTransaction);
        }

        public void Update(UserTransactionUpdateDTO userTransactionUpdateDTO)
        {
            UserTransaction userTransaction = _mapper.Map<UserTransaction>(userTransactionUpdateDTO);
            _userTransactionRepository.Update(userTransaction);
        }

        public void Delete(int id)
        {
            _userTransactionRepository.Delete(id);
        }
    }
}
