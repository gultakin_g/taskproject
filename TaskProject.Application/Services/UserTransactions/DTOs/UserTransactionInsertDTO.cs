﻿using System;

namespace TaskProject.Application.Services.UserTransactions.DTOs
{
    public class UserTransactionInsertDTO
    {
        public int UserId { get; set; }
        public double Amount { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
