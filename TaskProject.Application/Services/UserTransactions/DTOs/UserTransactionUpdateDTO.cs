﻿using System;

namespace TaskProject.Application.Services.UserTransactions.DTOs
{
    public class UserTransactionUpdateDTO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public double Amount { get; set; }
    }
}
