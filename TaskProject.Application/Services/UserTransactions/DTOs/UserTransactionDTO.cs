﻿using System;
using TaskProject.Application.Services.Users.DTOs;

namespace TaskProject.Application.Services.UserTransactions.DTOs
{
    public class UserTransactionDTO
    {
        public int Id { get; set; }
        public UserDTO User { get; set; }
        public double Amount { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
