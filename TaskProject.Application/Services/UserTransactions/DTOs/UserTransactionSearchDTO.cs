﻿using TaskProject.Application.Bases;

namespace TaskProject.Application.Services.UserTransactions.DTOs
{
    public class UserTransactionSearchDTO : BaseSearchDTO
    {
        public int? UserId { get; set; }
        public double? Amount { get; set; }
    }
}
