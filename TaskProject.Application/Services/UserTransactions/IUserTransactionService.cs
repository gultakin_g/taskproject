﻿using System.Collections.Generic;
using TaskProject.Application.Services.UserTransactions.DTOs;

namespace TaskProject.Application.Services.UserTransactions
{
    public interface IUserTransactionService
    {
        IList<UserTransactionDTO> FindList(UserTransactionSearchDTO userTransactionSearchDTO);
        UserTransactionUpdateDTO GetUserTransactionDetails(int id);
        void Insert(UserTransactionInsertDTO userTransactionInsertDTO);
        void Update(UserTransactionUpdateDTO userTransactionUpdateDTO);
        void Delete(int id); 
    }
}
