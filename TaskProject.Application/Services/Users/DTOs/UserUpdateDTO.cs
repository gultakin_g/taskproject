﻿using System.Collections.Generic;
using TaskProject.Application.Services.UserTransactions.DTOs;

namespace TaskProject.Application.Services.Users.DTOs
{
    public class UserUpdateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public IList<UserTransactionUpdateDTO> UserTransactions { get; set; }
    }
}
