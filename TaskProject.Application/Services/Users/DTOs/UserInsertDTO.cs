﻿using System.Collections.Generic;
using TaskProject.Application.Services.UserTransactions.DTOs;

namespace TaskProject.Application.Services.Users.DTOs
{
    public class UserInsertDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public  IList<UserTransactionInsertDTO> UserTransactions { get; set; }
    }
}
