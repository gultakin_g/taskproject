﻿using TaskProject.Application.Bases;

namespace TaskProject.Application.Services.Users.DTOs
{
    public class UserSearchDTO : BaseSearchDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
