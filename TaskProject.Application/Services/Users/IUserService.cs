﻿using System.Collections.Generic;
using TaskProject.Application.Services.Users.DTOs;
using TaskProject.Application.Services.UserTransactions.DTOs;

namespace TaskProject.Application.Services.Users
{
    public interface IUserService
    {
        UserDTO Authenticate(AuthDTO auth);
        IList<UserDTO> FindList(UserSearchDTO userSearchDTO);
        UserUpdateDTO GetUserDetails(int id);
        void Insert(UserInsertDTO userInsertDTO);
        void Update(UserUpdateDTO userUpdateDTO);
        void Delete(int id);
        IList<UserTransactionDTO> GetUserTransactions(int userId);
    }
}
