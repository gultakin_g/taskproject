﻿using AutoMapper;
using System.Collections.Generic;
using TaskProject.Application.Services.Users.DTOs;
using TaskProject.Application.Services.UserTransactions.DTOs;
using TaskProject.Domain.Models.Users;
using TaskProject.Domain.Models.UserTransactions;
using TaskProject.Infrastructure.Repositories.Users;

namespace TaskProject.Application.Services.Users
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public UserDTO Authenticate(AuthDTO authDTO)
        {
            Auth auth = _mapper.Map<Auth>(authDTO);
            User user = _userRepository.Authenticate(auth);
            UserDTO userDTO = _mapper.Map<UserDTO>(user);
            return userDTO;
        }

        public IList<UserDTO> FindList(UserSearchDTO userSearchDTO)
        {
            UserSearch userSearch = _mapper.Map<UserSearch>(userSearchDTO);
            IList<User> users = _userRepository.FindList(userSearch);
            IList<UserDTO> userDTOs = _mapper.Map<IList<UserDTO>>(users);
            return userDTOs;
        }

        public UserUpdateDTO GetUserDetails(int id)
        {
            User user = _userRepository.GetUserDetails(id);
            UserUpdateDTO userUpdateDTO = _mapper.Map<UserUpdateDTO>(user);
            return userUpdateDTO;
        }

        public void Insert(UserInsertDTO userInsertDTO)
        {
            User user = _mapper.Map<User>(userInsertDTO);
            _userRepository.Insert(user);
        }

        public void Update(UserUpdateDTO userUpdateDTO)
        {
            User user = _mapper.Map<User>(userUpdateDTO);
            _userRepository.Update(user);
        }

        public void Delete(int id)
        {
            _userRepository.Delete(id);
        }

        public IList<UserTransactionDTO> GetUserTransactions(int userId)
        {
            IList<UserTransaction> userTransactions = _userRepository.GetUserTransactions(userId);
            IList<UserTransactionDTO> userTransactionDTOs = _mapper.Map<IList<UserTransactionDTO>>(userTransactions);
            return userTransactionDTOs;
        }

        
    }
}
