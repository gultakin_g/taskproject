﻿using AutoMapper;
using TaskProject.Application.Services.Users.DTOs;
using TaskProject.Application.Services.UserTransactions.DTOs;
using TaskProject.Domain.Models.Users;
using TaskProject.Domain.Models.UserTransactions;

namespace TaskProject.Application
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            MapUsers();
            MapUserTransactions();
        }

        private void MapUsers()
        {
            CreateMap<AuthDTO, Auth>();
            CreateMap<User, UserDTO>();
            CreateMap<UserSearchDTO, UserSearch>();
            CreateMap<UserInsertDTO, User>();
            CreateMap<UserUpdateDTO, User>().ReverseMap();
        }

        private void MapUserTransactions()
        {
            CreateMap<UserTransaction, UserTransactionDTO>();
            CreateMap<UserTransactionSearchDTO, UserTransactionSearch>();

            CreateMap<UserTransactionInsertDTO, UserTransaction>()
                .ForMember(s => s.User, d => d.MapFrom(e => new User() { Id = e.UserId }));

            CreateMap<UserTransactionUpdateDTO, UserTransaction>()
                .ForMember(s => s.User, d => d.MapFrom(e => new User() { Id = e.UserId }))
                .ReverseMap()
                .ForMember(s => s.UserId, d => d.MapFrom(e => e.User != null ? e.User.Id : default(int)));
        }
    }
}
