﻿using FluentValidation;
using TaskProject.Application.Services.Users.DTOs;

namespace TaskProject.Application.Validators.Users
{
    public class UserInsertValidator : AbstractValidator<UserInsertDTO>
    {
        public UserInsertValidator()
        {
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required");
            RuleFor(e => e.Email).NotNull().NotEmpty().WithMessage("Eamil is required");
            RuleFor(e => e.Password).NotNull().NotEmpty().WithMessage("Password is required");
            RuleFor(e => e.UserTransactions).Must(e => e.Count > 0).WithMessage("UserTransations are required");
        }
    }
}
