﻿using FluentValidation;
using TaskProject.Application.Services.Users.DTOs;

namespace TaskProject.Application.Validators.Users
{
    public class UserUpdateValidator : AbstractValidator<UserUpdateDTO>
    {
        public UserUpdateValidator()
        {
            RuleFor(e => e.Id).GreaterThan(0).WithMessage("Id is required!");
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
            RuleFor(e => e.Email).NotNull().NotEmpty().WithMessage("Email is required!");
        }
    }
}
