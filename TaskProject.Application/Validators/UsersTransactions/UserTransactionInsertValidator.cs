﻿using FluentValidation;
using TaskProject.Application.Services.UserTransactions.DTOs;

namespace TaskProject.Application.Validators.UsersTransactions
{
    public class UserTransactionInsertValidator : AbstractValidator<UserTransactionInsertDTO>
    {
        public UserTransactionInsertValidator()
        {
            RuleFor(e => e.Amount).GreaterThan(0).WithMessage("Amount is required!");
            RuleFor(e => e.UserId).GreaterThan(0).WithMessage("UserId is required!");
        }
    }
}
