﻿using FluentValidation;
using TaskProject.Application.Services.UserTransactions.DTOs;

namespace TaskProject.Application.Validators.UsersTransactions
{
    public class UserTransactionUpdateValidator : AbstractValidator<UserTransactionUpdateDTO>
    {
        public UserTransactionUpdateValidator()
        {
            RuleFor(e => e.Id).GreaterThan(0).WithMessage("Id is required!");
            RuleFor(e => e.Amount).GreaterThan(0).WithMessage("Amount is required!");
            RuleFor(e => e.UserId).GreaterThan(0).WithMessage("UserId is required!");
        }
    }
}
