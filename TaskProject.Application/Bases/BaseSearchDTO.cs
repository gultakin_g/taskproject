﻿namespace TaskProject.Application.Bases
{
    public class BaseSearchDTO
    {
        public int CurrentPage { get; set; } = 1;
        public int PerPage { get; set; } = 5;
    }
}
