﻿using Microsoft.EntityFrameworkCore;
using TaskProject.Domain.Models.UserTransactions;
using TaskProject.Domain.Models.Users;

namespace TaskProject.Infrastructure
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("PK_Users_Id");
                entity.Property(e => e.Email).IsRequired();
                entity.HasAlternateKey(e => e.Email).HasName("UK_Users_Email");
                entity.Property(t => t.Name).HasMaxLength(30).IsRequired();
                entity.Property(t => t.Password).IsRequired();
                entity.HasMany(e => e.UserTransactions).WithOne(e => e.User).IsRequired();
                entity.ToTable("Users");
            });

            modelBuilder.Entity<UserTransaction>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("PK_User_Transactions_Id");
                entity.Property(e => e.Amount).IsRequired();
                entity.Property(e => e.CreatedAt).IsRequired().HasDefaultValueSql("getdate()");
                entity.HasOne(e => e.User).WithMany(e => e.UserTransactions).IsRequired();
                entity.ToTable("User_Transactions");
            });

            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1,
                    Name = "Gultakin Gasimova",
                    Email = "gultakin.beydullayeva@gmail.com",
                    Password = "123456"
                }
            );

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserTransaction> UserTransactions { get; set; }
    }
}
