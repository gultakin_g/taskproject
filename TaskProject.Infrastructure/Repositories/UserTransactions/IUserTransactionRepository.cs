﻿using System.Collections.Generic;
using TaskProject.Domain.Models.UserTransactions;

namespace TaskProject.Infrastructure.Repositories.UserTransactions
{
    public interface IUserTransactionRepository
    {
        IList<UserTransaction> FindList(UserTransactionSearch userTransaction);
        UserTransaction GetUserTransactionDetails(int id);
        void Insert(UserTransaction userTransaction);
        void Update(UserTransaction userTransaction);
        void Delete(int id);
    }
}
