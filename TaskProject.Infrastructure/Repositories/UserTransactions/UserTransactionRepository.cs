﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TaskProject.Domain.Models.UserTransactions;

namespace TaskProject.Infrastructure.Repositories.UserTransactions
{
    public class UserTransactionRepository : IUserTransactionRepository
    {
        private readonly DataContext _dataContext;

        public UserTransactionRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IList<UserTransaction> FindList(UserTransactionSearch userTransactionSearch)
        {
            IList<UserTransaction> userTransactions = _dataContext.UserTransactions
                .Skip((userTransactionSearch.CurrentPage - 1)* userTransactionSearch.PerPage)
                .Take(userTransactionSearch.PerPage)
                .Include(e => e.User)
                .ToList();
            return userTransactions;
        }

        public UserTransaction GetUserTransactionDetails(int id)
        {
            UserTransaction userTransaction = _dataContext.UserTransactions
                .Include(e => e.User)
                .FirstOrDefault(e => e.Id == id);
            return userTransaction;
        }

        public void Insert(UserTransaction userTransaction)
        {
            userTransaction.User = _dataContext.Users.First(e => e.Id == userTransaction.User.Id);
            _dataContext.UserTransactions.Add(userTransaction);
            _dataContext.SaveChanges();
        }

        public void Update(UserTransaction userTransaction)
        {
            userTransaction.User = _dataContext.Users.First(e => e.Id == userTransaction.User.Id);
            _dataContext.UserTransactions.Update(userTransaction);
            _dataContext.SaveChanges();
        }

        public void Delete(int id)
        {
            UserTransaction userTransaction = _dataContext.UserTransactions.First(e => e.Id == id);
            if (userTransaction != null)
            {
                _dataContext.UserTransactions.Remove(userTransaction);
                _dataContext.SaveChanges();
            }
        }
    }
}
