﻿using System.Collections.Generic;
using TaskProject.Domain.Models.Users;
using TaskProject.Domain.Models.UserTransactions;

namespace TaskProject.Infrastructure.Repositories.Users
{
    public interface IUserRepository
    {
        User Authenticate(Auth auth);
        IList<User> FindList(UserSearch userSearch);
        User GetUserDetails(int id);
        void Insert(User user);
        void Update(User user);
        void Delete(int id);
        IList<UserTransaction> GetUserTransactions(int userId);
    }
}
