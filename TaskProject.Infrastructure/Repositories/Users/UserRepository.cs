﻿using System.Collections.Generic;
using System.Linq;
using TaskProject.Domain.Models.Users;
using TaskProject.Domain.Models.UserTransactions;

namespace TaskProject.Infrastructure.Repositories.Users
{
    public class UserRepository : IUserRepository
    {
        public readonly DataContext _dataContext;

        public UserRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public User Authenticate(Auth auth)
        {
            User user = _dataContext.Users.FirstOrDefault(e => e.Email == auth.Email);
            
            if(user == null)
            {
                return null;
            }

            if(user.Password != auth.Password)
            {
                return null;
            }

            return user;
        }

        public IList<User> FindList(UserSearch userSearch)
        {
            IList<User> users = _dataContext.Users
                .Where(e => (userSearch.Name == null || e.Name == userSearch.Name) &&
                    (userSearch.Email == null || e.Email == userSearch.Email))
                .Skip((userSearch.CurrentPage - 1) * userSearch.PerPage)
                .Take(userSearch.PerPage)
                .ToList();
            return users;
        }

        public User GetUserDetails(int id)
        {
            User user = _dataContext.Users.FirstOrDefault(e => e.Id == id);
            if(user != null)
            {
                user.UserTransactions = _dataContext.UserTransactions
                    .Where(e => e.User.Id == user.Id).ToList();
            }
            return user;
        }

        public void Insert(User user)
        {
            _dataContext.Users.Add(user);
            _dataContext.SaveChanges();
        }

        public void Update(User user)
        {
            _dataContext.Users.Update(user);
            _dataContext.Entry(user).Property(e => e.Password).IsModified = false;
            _dataContext.Entry(user).Property(e => e.UserTransactions).IsModified = false;
            _dataContext.SaveChanges();
        }

        public void Delete(int id)
        {
            User user = _dataContext.Users.First(e => e.Id == id);
            if (user != null)
            {
                _dataContext.Users.Remove(user);
                _dataContext.SaveChanges();
            }
        }

        public IList<UserTransaction> GetUserTransactions(int userId)
        {
            IList<UserTransaction> userTransactions = _dataContext.UserTransactions
                .Where(e => e.User.Id == userId)
                .ToList();
            return userTransactions;
        }
    }
}
